|Branch|Status|
|------|:--------:|
|master|[![pipeline status](https://gitlab.itsupportme.by/ansible-cm/roles/prepare-sys/badges/master/pipeline.svg)](https://gitlab.itsupportme.by/ansible-cm/roles/prepare-sys/commits/master)
|develop|[![pipeline status](https://gitlab.itsupportme.by/ansible-cm/roles/prepare-sys/badges/develop/pipeline.svg)](https://gitlab.itsupportme.by/ansible-cm/roles/prepare-sys/commits/develop)

## Ansible Role
### **_prepare-sys_**

An Ansible Role that installs different packages on RedHat/CentOS, Debian/Ubuntu and Alpine Linux servers for more comfortable system usage.

## Requirements

  - Ansible 2.5 and higher

## Role Default Variables
```yaml
pve_integration_enabled: False    # Force JVM installation
```

## Dependencies

  - preconfig

## License

MIT

## Author Information

ITSupportMe, LLC
